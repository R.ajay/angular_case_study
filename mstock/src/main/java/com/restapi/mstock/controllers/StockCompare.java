package com.restapi.mstock.controllers;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.restapi.mstock.models.Stock;
import com.restapi.mstock.services.StockServices;

@RestController
@RequestMapping("api/stock")
@CrossOrigin
public class StockCompare {

	@Autowired
	StockServices stockServices;
	
	@RequestMapping(value = "/com1/{com1}/com2/{com2}/fromdate/{fromdate}/todate/{todate}", method = RequestMethod.GET)
	@ResponseStatus(HttpStatus.OK)
	public Map<String, List<Stock>> getstockcompare(@PathVariable(value = "com1") String com1, @PathVariable(value = "com2") String com2,@PathVariable(value = "fromdate")@DateTimeFormat(pattern = "yyyy-MM-dd") String fromdate,@PathVariable(value = "todate") @DateTimeFormat(pattern = "yyyy-MM-dd") String todate)
	{
		Map<String,List<Stock>> mp = new HashMap<String,List<Stock>>();

		
		List<Stock> varcom1= stockServices.getstockcompare(com1,fromdate,todate);
		List<Stock> varcom2= stockServices.getstockcompare(com2,fromdate,todate);
		
		mp.put("list1", varcom1);
		mp.put("list2", varcom2);
		return mp;
	}
	
	
	
}
