package com.restapi.mstock.controllers;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.restapi.mstock.models.UserLogin;
import com.restapi.mstock.services.UserAuthenticationService;

@RestController
@RequestMapping("/api/user")
public class UserControllers {

	@Autowired
	private UserAuthenticationService userService;
	@CrossOrigin
	@PostMapping("/login")
	@ResponseStatus(HttpStatus.OK)
		public boolean validate(@RequestBody UserLogin login)
		{
			System.out.println(login);
			boolean status = userService.validateUserLogin(login.getEmail(),login.getPassword());
			if(status)
				return status;
			else
			return false;
		}
	@GetMapping("/logout")
	public boolean logout(HttpSession session)
	{
		session.invalidate();
		
		return false;
	}
}
